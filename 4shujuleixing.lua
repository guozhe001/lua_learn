#!/usr/local/bin/lua

print("test table start...")
table1={key1="val1",key2="val2","val3"}
for k,v in pairs(table1) do
	print(k.."-"..v)
end

print("华丽丽的分隔符---------------------")
table1.key1 = nil
for k,v in pairs(table1) do
	print(k.."-"..v)
end
print("test table end...")
print("\r")

-------------------------------------------------------------------------------
print("test boolean start...")
print(type(false))
print(type(true))
print(type(nil))


if false or nil then
	print("这句话永远打印不出来")
else
	print("false and nil 都为false")
end
print("test boolean end...")
print("\r")

-------------------------------------------------------------------------------

print("test string start...")

html = [[
<html>
<head></head>
<body>
	<a href="https://www.google.com">google</a>
</body>
</html>
]]
print(html)

print("9"+10)
print('10'*0)
--print('这样会报错滴'+1)
print("hahah".."1".."nicai")
print(123 .. 456)

print(#html)

print(#'hahah')
print('test string end...')
print("\r")

-------------------------------------------------------------------------------


-------------------------------------------------------------------------------

print("test table start...")
-- 创建一个空的table
local table2 = {}

local table3 = {"晓军","海哥"}

for k,v in pairs(table3) do
	print(v)
end

for k in pairs(table3) do
	print(k)
end

print(table2)
print(table3)


local x = {}
x["key1"]="val1"
key1=10
x[key1]=22
x[key1]=x[key1] + 10

for k,v in pairs(x) do
	print(k..":"..v)
end


local a3 = {}
for i=1,10 do
	a3[i] = i
end
a3["key"]=1

for k,v in pairs(a3) do
	print(k,v)
end
print(a3[100])
print('test table end...')
print("\r")

-------------------------------------------------------------------------------


-------------------------------------------------------------------------------

print("test function start...")

function function1(n)
	if n == 0 then
		return 1
	else
		return n * function1(n-1)
	end
end
print(function1(5))

function2 = function1

print(function2(5))

--匿名函数
function anonymous_function(tab,fun)
	for k,v in pairs(tab) do
		print(fun(k,v));
	end
end

tab={key1="val1",key2="val2"}
anonymous_function(tab,function(key,val)
	return key ..":" ..val
end)
--匿名函数

print('test function end...')
print("\r")

-------------------------------------------------------------------------------
print("test bianliang start...")

table4 ={}
table4["haha"]="www.google.com"
print(table4.haha)
print(table4["haha"])

print('test bianliang end...')
print("\r")

-------------------------------------------------------------------------------




