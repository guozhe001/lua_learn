#!/usr/local/bin/lua


-------------------------------------------------------------------------------
print("test while start...")

a=10
while(a<=20) do
	print("a is: "..a)
	a = a+1
end

print("test while end...")
print("\r")

-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
print("test for start...")

table1 = {lua="learning",python="learning",java="what"}

for k,v in pairs(table1) do
	print(k..':'..v)
end

for i=1,10 do
	print("i is:"..i)
end

for i=100,10,-1 do
	print(i)
end

function f(x)
	print("in f")
	return x *10
end

for i=1,f(1) do
	print("test function for i is:"..i)
end

days = {"sunday","monday","tuesday","wednesday","thursday","friday","saturday"}
for k,v in pairs(days) do
	print(k,v)
end

print("test for end...")
print("\r")

-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
print("test repeat until start...")

a=10
repeat 
	print("do something! a is ".. a)
	a = a + 10
	until(a >100)

print("test repeat until end...")
print("\r")

-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
print("test all start...")

a=10
repeat 
	for i=1,a do
		print("i is : "..i)
	end
	print("do something! a is ".. a)
	a = a + 10
	until(a >100)

b=0
while(b < 10) do
	print("while"..b)
	repeat
		for i=1,b do
			print("for :"..i)
		end
		print("repeat"..b)
		b = b + 1
		until(b >5)
end

print("test all end...")
print("\r")

-------------------------------------------------------------------------------


